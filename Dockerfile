FROM centos
MAINTAINER n.ishii0101@gmail.com
RUN yum -y install epel-release && yum clean all
RUN yum -y install sshpass && yum clean all
RUN yum -y install ansible && yum clean all
RUN yum -y install python3-pip --enablerepo=epel && yum clean all
RUN mkdir /mydata
RUN mkdir /root/.ssh
COPY nob_common.lnk /root/.ssh/nob_common
RUN chmod 0600 /root/.ssh/nob_common
ENV LANG ja_JP.UTF-8
CMD ["/bin/bash"]